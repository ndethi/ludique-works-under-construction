<div class="hero-foot">
    <div class="content has-text-centered">
      <p class="has-text-weight-light">
        Ludique Works, Copyright &copy; <?php echo date('Y');  ?>
      </p>
      <p>&nbsp;</p>
    </div>
</div>
<footer>
    <script src="./public/js/hamburgerMenu.js"></script>
</footer>
