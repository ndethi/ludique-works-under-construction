<div class="hero-head">
  <div class="container">
      <nav class="navbar is-transparent">
          <div class="navbar-brand">
              <a class="navbar-item" href="/ludique">
                  <img src="public/img/lw-logomark-transparent-white.png" alt="" width="" height="">
              </a>
              <div class="navbar-burger burger" data-target="ludique-nav" id="nav-toggle" >
                  <span></span>
                  <span></span>
                  <span></span>
              </div>
          </div>
          <div id="ludique-nav" class="navbar-menu">
              <div class="navbar-end">
                <?php foreach($pageFolders as $page){?>
                  <a href="<?php echo current(explode(' ', basename($page)))?>" class="navbar-item"><?= basename($page)?></a>
                <?php } ?>

                  <a class="navbar-item is-hidden-desktop-only" href="https://twitter.com/ludiqueworks" target="_blank">
                      <span class="icon">
                          <i class="fab fa-lg fa-twitter"></i>
                      </span>
                  </a>
                  <a class="navbar-item is-hidden-desktop-only" href="https://facebook.com/ludiqueworks" target="_blank">
                      <span class="icon">
                          <i class="fab fa-lg fa-facebook"></i>
                      </span>
                  </a>
                  <a class="navbar-item is-hidden-desktop-only" href="https://youtube.com/ludiqueworks" target="_blank">
                      <span class="icon">
                          <i class="fab fa-lg fa-youtube"></i>
                      </span>
                  </a>
              </div>
          </div>
      </nav>
</div>
</div>
