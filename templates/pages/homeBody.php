<?php include('templates/default/meta.php') ?>
<html lang="en">
<body>
	<section class="hero hero-image is-dark is-fullheight">
		<?php include('templates/default/header.php') ?>
		<div class="hero-body">
			<div class="container">
				<div class="lw-banner-texts">
					<h1 class="title has-text-weight-bold ">
						We're out playing games.
					</h1>
					<h3 class="subtitle">
						<p>LudiqueWorks Co. is a pan-African game publishing company that makes global games and animated content,</p><p> majorly based on African themed content and tales.</p>
					</h3>
				</div>
				<div>&nbsp;</div>
			</div>
		</div>
    <?php include('templates/default/footer.php') ?>
</section>
</body>
</html>
