<section class="hero is-white is-medium">
	<?php @include('templates/partials/top/'.lcfirst(basename($request_uri)).'.php') ?>
		<div class="hero-body">
			<div class="container">
				<?php readPageCopy('./'."pages/".basename($request_uri).'/'.basename($request_uri).'.md"'); ?>
				<?php @include('templates/partials/bottom/'.lcfirst(basename($request_uri)).'.php') ?>
			</div>
		</div>
		<?php include('templates/default/footer.php') ?>
</section>
