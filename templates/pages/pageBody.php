<?php include('templates/default/meta.php') ?>
<?php include('templates/pages/styles/pageStyle.php') ?>
<html lang="en">
<body>
	<section class="hero is-dark is-medium" id="<?php echo basename($request_uri); ?>_image">
		<?php include('templates/default/header.php') ?>
		<div class="hero-body">
			<div class="container">
					<h1 class="title has-text-weight-bold"><?php echo basename($request_uri) ?></h1>
					<h3 class="subtitle">

					</h3>
				<div>&nbsp;</div>
			</div>
		</div>

</section>
<?php include('templates/pages/pageCopy.php') ?>
</body>
</html>
