<div class="hero-body">
  <div class="container">
    <div class="columns">
      <?php foreach($teamProfiles as $profile){ ?>
      <div class="column">
        <div class="box">
          <figure class="image">
            <img src="<?='./'."pages/Team/profiles/".basename($profile).'/'.strtolower(str_replace(' ', '',basename($profile))).'.jpeg' ?>" alt="<?php echo basename($profile); ?>">
          </figure>
        </div>
        <?php readPageCopy('./'."pages/Team/profiles/".basename($profile).'/'.strtolower(str_replace(' ', '',basename($profile))).'.md"'); ?>
      </div>
    <?php }?>
    </div>
  </div>
</div>
