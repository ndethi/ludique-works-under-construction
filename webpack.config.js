module.exports = {
  entry: './public/js/app.js',
  output: {
    path: __dirname + '/public/dist',
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
      test: /\.css$/,
      loaders: [
        'style-loader',
        'css-loader',
      ]
    },
    {
      test: /\.(png|svg|jpg|gif)$/,
      loaders: [
        'file-loader'
      ]
    },
    {
      test: /\.(scss|sass)$/,
      loaders: [
        'style-loader',
        'css-loader',
        'sass-loader',
      ]
    },
    {
      test: /\.(woff|woff2|eot|ttf|otf)$/,
      loaders: [
        'file-loader',
      ]
    }
    ]
  }
}