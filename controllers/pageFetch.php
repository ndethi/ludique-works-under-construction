<?php

//Include parsedown
require 'vendor/Parsedown.php';

//Get all pages folders
function getPageFolders(){
  $GLOBALS['pageFolders'] = glob('pages/*' , GLOB_ONLYDIR);
}

//get all images and copy
$GLOBALS['pageImages'] = [];
$GLOBALS['pageCopy'] = [];
$GLOBALS['teamProfiles'] = [];

//Get all page content for this page
function getPageCopy($page){
  foreach(glob("pages/".$page."/*.md") as $copy){
    array_push($GLOBALS['pageCopy'], $copy);
  }
  echo $GLOBALS['pageCopy'][0];
}

function getPageImage($page){
  foreach(glob("pages/".$page."/*.jpg") as $copy){
     array_push($GLOBALS['pageImages'],$copy);
  }
  echo $GLOBALS['pageImages'][0];
}

function readPageCopy($copy){
  $openedCopy = file_get_contents($copy);

  //Read Markdown
  $parsedown = new Parsedown();
  echo $parsedown->text($openedCopy);
}

function getTeamProfiles(){
  $GLOBALS['teamProfiles'] = glob('pages/Team/profiles/*');
}


getTeamProfiles();
getPageFolders();
require_once('pageRoute.php');


 ?>
