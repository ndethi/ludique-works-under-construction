<table>
	<thead></thead>
	<tbody>
		<tr>
			<td valign="top" style="padding-top: 0; padding-bottom: 0; padding-left: 0; padding-right: 7px; border-top: 0; border-bottom: 0: border-left: 0;" >
				<img height="80px" src="https://png.icons8.com/silhouette-person">
			</td>
			<td style="padding-top: 0; padding-bottom: 0; padding-left: 12px; padding-right: 0;">
			<table cellpadding="0" cellspacing="0" border="0" style="background: none; border-width: 0px; border: 0px; margin: 0; padding: 0;" >
				<tr><td colspan="2" style="padding-bottom: 0px; color: #000000; font-size: 16px; font-weight: bold; font-family: Arial, Helvetica, sans-serif;">Nathan Masyuko</td></tr>
				<tr><td colspan="2" style="padding-bottom: 5px; color: #000000; font-size: 16px; font-family: Arial, Helvetica, sans-serif;">Head of Gaming</td></tr>
				<tr><td colspan="2" style="padding-bottom: 2px; color: #888888; font-size: 14px; font-family: Arial, Helvetica, sans-serif;"><hr></td></tr>
				<tr><td colspan="2" style="padding-bottom: 2px; color: #888888; font-size: 14px; font-family: Arial, Helvetica, sans-serif;"><img src="https://png.icons8.com/phone" height="17">&nbsp;&nbsp;+254 700 000</td></tr>
				<tr><td colspan="2" style="padding-bottom: 2px; color: #888888; font-size: 14px; font-family: Arial, Helvetica, sans-serif;"><img src="https://png.icons8.com/email" height="17">&nbsp;&nbsp;<a style="color: #808080; text-decoration: none;href="mailto:masyuko@gmail.com">masyuko@gmail.com</a></td></tr>
				<tr><td colspan="2" style="padding-bottom: 2px; color: #888888; font-size: 14px; font-family: Arial, Helvetica, sans-serif;"><img src="https://png.icons8.com/skype" height="17">&nbsp;&nbsp;nmasyuko</td></tr>
				<tr><td colspan="2" style="padding-bottom: 2px; color: #888888; font-size: 14px; font-family: Arial, Helvetica, sans-serif;"><hr></td></tr>
				<tr><td colspan="2" style="padding-bottom: 2px; color: #888888; font-size: 14px; font-family: Arial, Helvetica, sans-serif;"><img src="https://png.icons8.com/internet" height="17">&nbsp;&nbsp;<a style="color: #808080; text-decoration: none; href="http://www.ludique.works">www.ludique.works</a></td></tr>
				<tr><td colspan="2" ><hr></td></tr>
				<tr>
					<td colspan="1" style="padding-bottom: 2px; color: #888888; font-size: 14px; font-family: Arial, Helvetica, sans-serif;"><a href="twitter.com/ludiqueworks"><img src="https://png.icons8.com/twitter" height="17"></a></td>
					<td colspan="2" style="padding-bottom: 2px; color: #888888; font-size: 14px; font-family: Arial, Helvetica, sans-serif;"><a href="facebook.com/ludiqueworks"><img src="https://png.icons8.com/facebook" height="17"></a></td>
					<td colspan="2" style="padding-bottom: 2px; color: #888888; font-size: 14px; font-family: Arial, Helvetica, sans-serif;"><a href="youtube.com/ludiqueworks"><img src="https://png.icons8.com/youtube" height="17"></a></td>			
				</tr>
				<tr>
					<td colspan="2"><img id="preview-image-url" src="https://drive.google.com/uc?id=1CjlL6Pucn2bIet-sZ4fPHZJG7x1A4kpb" height="100"></td>
				</tr>
			</table>
		</td>	
		</tr>
		<tr></tr>
	</tbody>
</table>


