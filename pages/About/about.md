**LudiqueWorks** is the continent's fastest growing game publisher, forging the continent's largest network of video game developer studios  and gaming communities.

Our aim is to lead the video game industry in Africa, leading the continent in the telling of our stories through interactive media to the rest of the world.
