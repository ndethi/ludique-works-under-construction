The Africa Game Developers Community spans 12+ African countries and growing.

Our membership consists of members from: Kenya, Uganda, Tanzania, Rwanda, Ethiopia, Cameroon, Ghana, Senegal, Ivory Coast, Zambia, Zimbabwe and South Africa.

We host Workshops, Talks and Demo Days and trainings to enable the Growth and Sustainability of African studios. The membership continues to grow.

Sign up [here](http://bit.ly/devsInfo) to be part of the community and Follow us on [Twitter](https://twitter.com/ludiqueworks ) and [Medium](https://medium.com/ludiqueworksmedia)
